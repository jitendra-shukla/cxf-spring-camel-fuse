package test.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.provider.JAXBElementProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.model.Customer;


@RunWith(JUnit4.class)
public class RsApplicationTests {
	WebClient client;
	
	@Before
	public void createWebClient() {
		client = WebClient.create("http://localhost:9000/route", Arrays.asList(jacksonJaxbJsonProvider(),jaxbElementProvider()));
	}
	
	@Test
	public void testCustomerPagination() {
		@SuppressWarnings("unchecked")
		List<Customer> customers = client.path("customers")
		      .accept(MediaType.APPLICATION_JSON)
			  .query("start", 0)
			  .query("size", 2)
		      .get(List.class);
		
		assertEquals(2, customers.size());
	}
	
	@Test
	public void testAddCustomer() {
		Customer customer = client.path("customers")
		      .accept(MediaType.APPLICATION_JSON)
			  .header("Content-Type", MediaType.APPLICATION_JSON)
		      .post(new Customer(4, "Jitendra Shukla", "HYD"), Customer.class);
		
		assertEquals("Jitendra Shukla", customer.getName());
	}
	
	@Test
	public void testGetCustomer() {
		Customer customer = client.path("customers")
		      .accept(MediaType.APPLICATION_JSON)
		      .path(3)
		      .get(Customer.class);
		
		assertEquals(3, customer.getId());
	}
	
	@Test
	public void testGetAllCustomer() {
		@SuppressWarnings("unchecked")
		List<Customer> customers = client.path("customers")
		      .accept(MediaType.APPLICATION_JSON)
		      .get(List.class);
		
		assertFalse(customers.isEmpty());
	}
	
	@Test
	public void testEditCustomer() {
		Customer customer = client.path("customers")
		      .accept(MediaType.APPLICATION_JSON)
			  .header("Content-Type", MediaType.APPLICATION_JSON)
		      .put(new Customer(4, "Jitendra Shukla", "HYDERABAD"), Customer.class);
		
		assertEquals("HYDERABAD", customer.getAddress());
	}
	
	@Test
	public void testDeleteCustomer() {
		Response customer = client.path("customers")
		      .accept(MediaType.APPLICATION_JSON)
		      .path(4)
		      .delete();
		
		assertEquals(200, customer.getStatus());
	}
	
	 @Bean
	 public JacksonJaxbJsonProvider jacksonJaxbJsonProvider() {
	        return new JacksonJaxbJsonProvider();
	 }

	 @Bean
	 public JAXBElementProvider<Customer> jaxbElementProvider() {
	        return new JAXBElementProvider<Customer>();
	 }

}
