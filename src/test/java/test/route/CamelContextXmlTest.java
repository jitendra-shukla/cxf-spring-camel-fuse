package test.route;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.apache.cxf.jaxrs.impl.ResponseImpl;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CamelContextXmlTest extends CamelSpringTestSupport {

	@Produce(uri = "cxfrs://bean://rsClient")
	protected ProducerTemplate inputEndpoint;

	@Test
	public void testCamelRoute() throws Exception {

		Map<String, Object> headers = new HashMap<String,Object>();
		headers.put("method", "POST");
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		
		ResponseImpl cust = (ResponseImpl) inputEndpoint.requestBodyAndHeaders("{\"id\": 1, \"name\": \"Jitendra HYD\",\"address\": \"Madhapur\"}", headers);		
		System.out.println(cust.getStatus());
		
		assertEquals(200, cust.getStatus());
		
		assertMockEndpointsSatisfied();
	}

	@Override
	protected ClassPathXmlApplicationContext createApplicationContext() {
		return new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
	}

}
