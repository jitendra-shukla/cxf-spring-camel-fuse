package exception;

public class DataNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1152164144273611741L;
	
	public DataNotFoundException(String message) {
		super(message);
	}

}
