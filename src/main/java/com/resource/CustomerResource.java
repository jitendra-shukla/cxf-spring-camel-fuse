package com.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Service;

import com.model.Customer;
import com.wordnik.swagger.annotations.*;


@Service
@Path("customers")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/customers", description = "Operations about customerservice")
public interface CustomerResource {
	
	@GET
	public List<Customer> getCustomers(@QueryParam("start") int start, @QueryParam("size") int size);
	
	@GET
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Customer getCustomer(@PathParam("id") int id);
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	//@Valid
	@ApiOperation(value = "Find Customer by ID", notes = "More notes about this method", response = Customer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Invalid ID supplied"),
            @ApiResponse(code = 204, message = "Customer not found")
            })
	public Customer addCustomer(Customer customer);
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update an existing Customer")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Invalid ID supplied"),
            @ApiResponse(code = 204, message = "Customer not found")
            })
	public Customer modifyCustomer(Customer customer);
	
	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Delete Customer")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Invalid ID supplied"),
            @ApiResponse(code = 204, message = "Customer not found")
            })
	public Customer deleteCutomer(@PathParam("id") int id);	

}

