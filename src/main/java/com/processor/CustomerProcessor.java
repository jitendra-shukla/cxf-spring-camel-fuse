package com.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
//import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.cxf.message.MessageContentsList;

import com.db.DbMap;
import com.model.Customer;

import exception.DataNotFoundException;

public class CustomerProcessor implements Processor{
	
	private static final String GET="GET";
	private static final String PUT="PUT";
	private static final String POST="POST";
	private static final String DELETE="DELETE";

	private static Map<Integer, Customer> customers = DbMap.getCustomers();
	@Override
	public void process(Exchange exchange) throws Exception {

		Message inMessage = exchange.getIn();      
		System.out.println("In Processor");
        String httpMethod = inMessage.getHeader(Exchange.HTTP_METHOD, String.class);
        if(GET.equals(httpMethod)) {
        	MessageContentsList cxfMessage = exchange.getIn().getBody(MessageContentsList.class);
			if (cxfMessage.size() == 2) {
				int start = Integer.valueOf(cxfMessage.get(0).toString());
				int size = Integer.valueOf(cxfMessage.get(1).toString());
				if (size > 0) {
					exchange.getOut().setBody(getPaginatedCustomer(start, size));
				} else {
					exchange.getOut().setBody(new ArrayList<>(customers.values()));
				}
			}else {
				int id = Integer.valueOf(cxfMessage.get(0).toString());
				if(!customers.containsKey(id)) {
	    			throw new DataNotFoundException("Customer with Id " + id + " not found");
	    			}
				exchange.getOut().setBody(customers.get(id));
			}
        }
        
        if(POST.equals(httpMethod)) {
        	Customer customer = inMessage.getBody(Customer.class);
        	customer.setId(customers.size() + 1);
    		customers.put(customer.getId(), customer);
    		exchange.getOut().setBody(customer);
        }
        
        if(PUT.equals(httpMethod)) {
        	Customer customer = inMessage.getBody(Customer.class);
        	if(!customers.containsKey(customer.getId())) {
    			throw new DataNotFoundException("Customer with Id " + customer.getId() + " not found");
    			} 
    		customers.put(customer.getId(), customer);
    		exchange.getOut().setBody(customer);
        }
        
        if(DELETE.equals(httpMethod)) {
        	int id = inMessage.getBody(Integer.class);
        	if(!customers.containsKey(id)) {
    			throw new DataNotFoundException("Customer with Id " + id + " not found");
    			}
    		Customer customer = customers.get(id);
    		customers.remove(id);
    		exchange.getOut().setBody(Response.ok().entity(customer).build());
        }
	}
	
	private List<Customer> getPaginatedCustomer(int start, int size) {
		if(start+size > customers.size() ) {
			return new ArrayList<>(customers.values()).subList(start, customers.size());
		}else {
			return new ArrayList<>(customers.values()).subList(start, size + start);
		}
	}

}
