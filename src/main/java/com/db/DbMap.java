package com.db;

import java.util.HashMap;
import java.util.Map;

import com.model.Customer;


public class DbMap {
	
	private static Map<Integer, Customer> customers = initCustomer();

	private static Map<Integer, Customer> initCustomer() {
		
		Map<Integer, com.model.Customer> customerMap= new HashMap<>();
		customerMap.put(1, new Customer(1, "Jitendra", "Madhapur"));
		customerMap.put(2, new Customer(2, "Ashu", "Madhapur"));
		customerMap.put(3, new Customer(3, "Atul", "DDN"));
		return customerMap;
	}

	public static Map<Integer, Customer> getCustomers() {
		return customers;
	}
}
