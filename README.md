# README #

CXF REST, Apache Camel and Spring integration. Runtime environment is JBoss Fuse

### What is this repository for? ###

This example shows how to work with a Apache CXF application using Spring and Camel.

REST endpoints are exposed and are bind to Camel routes. Since endpoints are bind to Camel routes, implementation is blank.

### Software used ###

Fuse runtime 6.3  
JBoss Studio 11.1.0 GA  
Camel-core 2.17.0.redhat-630187  
Camel-spring 2.17.0.redhat-630187  
Swagger 2.18.1.redhat-000021  

The example performs CRUD operation using REST API. CXF WebClient Api is used for JUnit test.

### How to run ###

1. Start fuse server
2. Install camel-jackson feature using below command  
   ```features:install camel-jackson ```
3. Install application bundle using below command  
   ```osgi:install mvn:com.mycompany/camel-spring-test/1.0.0-SNAPSHOT ```
4. ```osgi:install ```command will return a bundle id. Run below command and pass the bundle id to start the bundle  
   ``` osgi:start <bundle-id> ```
   
## Exception handling ##  

ExceptionMapper generic interface have been used to map exception and prepare exception message in JSON format.
  

## How to test ##

JUnit is written for both Routes and REST API.  

To test the routes, stop Fuse server and run the JUnit test case.  

To test the API, start Fuse server and make sure application is running. After that run the JUnit test cases.   
CXF WebClient API is used for JUnit test.   

We can test with REST client tool like Postman  

API Endpoint http://localhost:9000/route/customers

### Swagger API UI URL ###
http://localhost:9000/route/api-docs?url=/api/swagger.json#/Customer


